#pragma once
#include "PlainText.h"
#include <iostream>


class SubstitutionText : public PlainText
{
	private:
		std::string _dictionaryFileName;
	public:
		SubstitutionText(std::string& txt, std::string& dictName);
		~SubstitutionText();
	
		/**
		 * @brief encrypts this.text field
		 * @return encrypted text
		*/
		std::string encrypt();

		/**
		 * @brief decrypts this.text field
		 * @return decrypted field
		*/
		std::string decrypt();
	
		friend std::ostream& operator<<(std::ostream& os, const SubstitutionText& txt);
};