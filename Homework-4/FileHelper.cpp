#include "FileHelper.h"
#include <string>
#include <fstream>
#include <functional>
#include <iostream>
#include <streambuf>

std::string FileHelper::readFileToString(const std::string& fileName)
{
	std::ifstream t(fileName);
	std::string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	
	return str;
}

void FileHelper::write_words_to_file(const std::string& inputFileName, const std::string& outputFileName)
{
	std::fstream in_file;
	std::string word;

	in_file.open(inputFileName);
	
	std::ofstream out_file;
	out_file.open(outputFileName);
	
    while (in_file >> word)
    {
		word.erase(std::remove_if(word.begin(), word.end(), std::not1(std::ptr_fun(std::isalnum))), word.end());
		out_file << word << "\n";
    }
	out_file.close();
	in_file.close();
}
