#pragma once
#include "PlainText.h"
#include <iostream>


class BonusText : public PlainText
{
	public:
		BonusText(std::string& str);
		~BonusText();
		std::string decrypt(const std::string& path_to_decrypter);
		friend std::ostream& operator<<(std::ostream& os, const BonusText& txt);
};


