#pragma once
#include <iostream>


class PlainText
{
	protected:
		std::string text;
		bool isEncrypted;
		static int _NumOfTexts;

	public:
		PlainText(std::string& str);
		~PlainText();

		// simple getters
		bool isEnc() const;
		std::string getText() const;
		static int get_num_texts();

		friend std::ostream& operator<<(std::ostream& os, const PlainText& txt);
};



