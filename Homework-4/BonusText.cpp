#include "BonusText.h"
#include "FileHelper.h"
#include <fstream>

BonusText::BonusText(std::string& str) : PlainText(str) {};
BonusText::~BonusText() {};=

/**
 * @brief decrypts this.text using decrypter at given path
 * @param path_to_decrypter path to folder containing decrypter
 * @return decrypted text
*/
std::string BonusText::decrypt(const std::string& path_to_decrypter)
{
	/*std::ofstream out_file;
	out_file.open(path_to_dectypter + "/encrypted.txt");
	out_file << this->text;
	out_file.close();*/

	// create command for execution
	std::string command = "cd " + path_to_decrypter + " && python decrypt.py";
	// execute the command
	system(command.c_str());

	// create path to file with decrypted text
	std::string path_to_decrypter_exe = path_to_decrypter + "/decrypted.txt";
	// get decrypted text from file
	this->text = FileHelper::readFileToString(path_to_decrypter_exe);

	return this->text;
}

std::ostream& operator<<(std::ostream& os, const BonusText& txt)
{
	std::ofstream log_file;
	log_file.open("log.txt");
	log_file << txt.text;
	
	return os;
}
