#include "ShiftText.h"


ShiftText::ShiftText(std::string& str, const int num) : PlainText(str), _key(num) {};

ShiftText::~ShiftText() {};

/**
 * @brief encrypts this.text according to this.key
 * @return encrypted text 
*/
std::string ShiftText::encrypt()
{
	// temp variable to store the current working char
	char letter;
	// loop through every char in this.text string
	for (int i = 0; i < this->text.length(); i++)
	{
		letter = this->text[i];
		// encrypt for lowercase letter
		if (letter >= 'a' && letter <= 'z') {
			letter = letter + this->_key;
			if (letter > 'z') {
				letter = letter - 'z' + 'a' - 1;
			}
			this->text[i] = letter;
		}
		// encrypt for uppercase letter
		else if (letter >= 'A' && letter <= 'Z') {
			letter = letter + this->_key;
			if (letter > 'Z') {
				letter = letter - 'Z' + 'A' - 1;
			}
			this->text[i] = letter;
		}
	}
	this->isEncrypted = true; // change isEncrypted flag to true
	return this->text;
}

/**
 * @brief decrypts this.text according to this.key
 * @return decrypted text
*/
std::string ShiftText::decrypt()
{
	// temp variable to store the current working char
	char letter;
	// loop through every char in this.text string
	for (int i = 0; i < this->text.length(); i++)
	{
		// encrypt for lowercase letter
		letter = this->text[i];
		if (letter >= 'a' && letter <= 'z') {
			letter = letter - this->_key;
			if (letter < 'a') {
				letter = letter + 'z' - 'a' + 1;
			}
			this->text[i] = letter;
		}
		// decrypt for uppercase letter
		else if (letter >= 'A' && letter <= 'Z') {
			letter = letter - this->_key;
			if (letter < 'A') {
				letter = letter + 'Z' - 'A' + 1;
			}
			this->text[i] = letter;
		}
	}
	this->isEncrypted = false; // change isEncrypted flag to true
	return this->text;
}

std::ostream& operator<<(std::ostream& os, const ShiftText& txt)
{
	os << "Shift Text" << std::endl << txt.text;
	return os;
}
